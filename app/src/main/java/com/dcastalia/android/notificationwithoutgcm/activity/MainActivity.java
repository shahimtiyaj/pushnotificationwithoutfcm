package com.dcastalia.android.notificationwithoutgcm.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dcastalia.android.notificationwithoutgcm.R;
import com.dcastalia.android.notificationwithoutgcm.service.NotificationSchedulerService;
import com.dcastalia.android.notificationwithoutgcm.utils.Utils;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService(new Intent(MainActivity.this, NotificationSchedulerService.class));
        Utils.log("Start Service: " + "NotificationSchedulerService");
    }
}
