package com.dcastalia.android.notificationwithoutgcm.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.dcastalia.android.notificationwithoutgcm.utils.Utils;

import java.util.Calendar;

public class NotificationSchedulerService extends Service {

    // restart service every 30 seconds
    private static final long REPEAT_TIME = 1000 * 30;

    public NotificationSchedulerService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            AlarmManager service = (AlarmManager) getApplicationContext()
                    .getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(getApplicationContext(), StartNotificationServiceReceiver.class);
            PendingIntent pending = PendingIntent.getBroadcast(getApplicationContext(), 0, i,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            Calendar cal = Calendar.getInstance();
            // start 30 seconds after boot completed
            cal.add(Calendar.SECOND, 30);
            // fetch every 30 seconds
            // InexactRepeating allows Android to optimize the energy consumption
            service.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    cal.getTimeInMillis(), REPEAT_TIME, pending);

            Utils.log("Start Receiver: " + "StartNotificationServiceReceiver");
            // service.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
            // REPEAT_TIME, pending);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
