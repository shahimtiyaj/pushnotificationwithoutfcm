package com.dcastalia.android.notificationwithoutgcm.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dcastalia.android.notificationwithoutgcm.utils.Utils;

public class StartNotificationServiceReceiver extends BroadcastReceiver {

    public StartNotificationServiceReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent service = new Intent(context, NotificationService.class);
            context.startService(service);
            Utils.log("Start Service: " + "NotificationService");
        }catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
